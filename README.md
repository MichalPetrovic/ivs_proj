![Scheme](screenshot.png)

# IVS Project 2

A simple calculator with GUI, manual, documentation and it's own mathematical library and installer for Windows and Debian-based linux.

## About

The project was intended to show us the entire process of creating software while also teaching us the basics of working in a team.

### Environment

Ubuntu 32/64bit
Windows 32/64bit

### Authors

* **Michal Petrovič**
* **Patrik Kupčo**
* **Juraj Záhradníček**

### Assignment

The project assignment can be found [here](http://ivs.fit.vutbr.cz/projekt-2_tymova_spoluprace2018-19.html)

## Getting started

If you do not want edit or view the code, please download installation packages.

```
git clone https://MichalPetrovic@bitbucket.org/MichalPetrovic/ivs_proj.git
```
## Install from package

### Prerequisites

All the required software should be included.

### Installation

#### Windows

Download the .exe package [here](https://bitbucket.org/MichalPetrovic/ivs_proj/downloads/calculator-SZF.exe) and just install it.

#### Debian-based Linux

Download the .deb package [here](https://bitbucket.org/MichalPetrovic/ivs_proj/downloads/calculator-SZF.deb)

Navigate to the folder where the package is and 

```
apt install ./calculator-SZF.deb
```
Then you can run program from start menu, or from console
```
calculator-SZF
```

## Built With
* [Python](https://www.python.org/) - The programming language used
* [PyQt](https://wiki.python.org/moin/PyQt) - Used for the GUI

## License
This project is licensed under the GNU GPL v.3 License.