import sys
import os
import PyQt5
import calculator_ui
import math_lib


##
# @brief Class that bridges math_lib with calculator_ui


class AppWindow(PyQt5.QtWidgets.QDialog, calculator_ui.Ui_Calculator_Main):


    ##
    # @brief Initializes the calculator


    def __init__(self):
        super().__init__()
        self.setupUi(self)
        scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(
            PyQt5.QtGui.QIcon(
                scriptDir + os.path.sep + "favicon.png"))

        self.btn_num_1.clicked.connect(self.button_pressed)
        self.btn_num_2.clicked.connect(self.button_pressed)
        self.btn_num_3.clicked.connect(self.button_pressed)
        self.btn_num_4.clicked.connect(self.button_pressed)
        self.btn_num_5.clicked.connect(self.button_pressed)
        self.btn_num_6.clicked.connect(self.button_pressed)
        self.btn_num_7.clicked.connect(self.button_pressed)
        self.btn_num_8.clicked.connect(self.button_pressed)
        self.btn_num_9.clicked.connect(self.button_pressed)
        self.btn_num_0.clicked.connect(self.button_pressed)
        self.btn_num_dot.clicked.connect(self.button_pressed)
        self.btn_num_pi.clicked.connect(self.button_pressed)

        self.btn_plus.clicked.connect(self.button_pressed)
        self.btn_minus.clicked.connect(self.button_pressed)
        self.btn_mul.clicked.connect(self.button_pressed)
        self.btn_div.clicked.connect(self.button_pressed)
        self.btn_pow.clicked.connect(self.button_pressed)
        self.btn_root.clicked.connect(self.button_pressed)
        self.btn_factorial.clicked.connect(self.button_pressed)
        self.btn_open_paren.clicked.connect(self.button_pressed)
        self.btn_close_paren.clicked.connect(self.button_pressed)
        self.btn_clear.clicked.connect(self.button_ce_pressed)
        self.btn_equal.clicked.connect(self.button_equal_pressed)
        self.btn_backspace.clicked.connect(self.button_bcs_pressed)

        self.expr_box.returnPressed.connect(self.button_equal_pressed)
        self.expr_box_convertor.returnPressed.connect(self.button_convert_pressed)

        self.btn_convert.clicked.connect(self.button_convert_pressed)

        self.show()
        self.expr_box.setFocus(True)


    ##
    # @brief Opens the converter window when button is pressed


    def button_convert_pressed(self):
        expression = self.expr_box_convertor.text()
        self.result_bin.setText(math_lib.convert(expression, "bin"))
        self.result_oct.setText(math_lib.convert(expression, "oct"))
        self.result_hex.setText(math_lib.convert(expression, "hex"))


    ##
    # @brief Deletes the last character on the display


    def button_bcs_pressed(self):
        self.expr_box.setText(self.expr_box.text()[:-1])


    ##
    # @brief Inserts the pressed number/button into the display


    def button_pressed(self):
        btn = self.sender()
        expression = self.expr_box.text() + btn.text()
        self.expr_box.setText(expression)


    ##
    # @brief Clears the whole display


    def button_ce_pressed(self):
        self.expr_box.setText("")
        self.result_box.setText("")


    ##
    # @brief Passes the expression on the display into the math_lib and
    # shows the returning result


    def button_equal_pressed(self):
        try:
            result = math_lib.parser(self.expr_box.text())
            if float(result).is_integer():
                self.result_box.setText(str(int(float(result))))
            else:
                self.result_box.setText(result)
        except IndexError:
            self.result_box.setText("SYN ERROR")
        except ValueError:
            self.result_box.setText("SYN ERROR")
        except:
            self.result_box.setText("MA ERROR")


if __name__ == "__main__":
    app = PyQt5.QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")
    window = AppWindow()
    window.show()
    sys.exit(app.exec_())
