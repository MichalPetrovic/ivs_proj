#!/usr/bin/python3
# -*- coding: utf-8 -*-


##
# @mainpage Introduction
# Welcome to the software documentation to our calculator.
#
# @section ToC Table of Contents
#
# - @ref basic_operations
#
#   - @ref math_lib.add "Addition [add()]"
#   - @ref math_lib.sub "Subtraction [sub()]"
#   - @ref math_lib.mul "Multiplication [mul()]"
#   - @ref math_lib.div "Division [div()]"
#
# - @ref advanced_operations
#
#  - @ref math_lib.u_pow "Exponentiation [u_pow()]"
#  - @ref math_lib.u_rt "n-th root [u_rt()]"
#  - @ref math_lib.factorial "Factorial [factorial()]"
#
# - @ref converter
#
#   - @ref math_lib.convert "Base Converter [convert()]"
#
# - @ref parentheses
#
#    - @ref math_lib.execute 
#    - @ref math_lib.parser


##
# @package math_lib
#
# @brief "math_lib" is a mathematical library used in our calculator.
#
# It is capable of basic (addition, subtraction, multiplication,
# division) and more advanced (exponentiation, n-th root and factorial)
# mathematical operations. The library is also capable of counting
# expressions inside brackets and converting a number from base 10
# numeral system into base 2, 8 and 16 numeral systems.
#
import re


##
# @brief Sets "math error" exception.


def set_err():
    raise Exception("math error")


##
# @defgroup basic_operations Basic Operations
# @brief Includes basic mathemathical operations (addition, subtraction,
# multiplication and division).
# @{

##
# @brief Adds two numbers together
#
# @param x First number
# @param y Second number
#
# @return Sum of two numbers


def add(x, y):
    return round(x + y, 10)


##
# @brief Subtracts one number from another.
#
# @param x Number it is subtracted from (minuend)
# @param y Number being subtracted (subtrahend)
#
# @return Difference of the two numbers.

def sub(x, y):
    return round(x - y, 10)


##
# @brief Multiplies two numbers together.
#
# @param x First number (multiplier)
# @param y Second number (multiplicand)
#
# @return Product of two numbers.


def mul(x, y):
    return round(x * y, 10)


##
# @brief Divides one number by another.
#
# @param x First number (dividend)
# @param y Second number (divisor)
#
# @exception "math error" if the second number (divisor) is 0
#
# @return Quotient of two numbers.


def div(x, y):
    return round(x / y, 10) if y != 0 else set_err()

##
# @}

##
# @defgroup advanced_operations Advanced Operations
# @brief Includes advanced mathematical operations (exponentiation,
# n-th root and factorial).
# @{


##
# @brief Raises base to the power of n.
#
# @param x Base number
# @param y exponent
#
# @exception "math error" if the base number is 0 and exponent is a negative
# number
#
# @return Base raised to the power of n.


def u_pow(x, y):
    return set_err() if x == 0 and y < 1 else round(x ** y, 10)


##
# @brief Calculates nth(degreeth) root of a radicant.
#
# @param x Radicant
# @param y Degree
#
# @exception "math error" if radicant is equal to zero and
# degree is lower than zero
# @exception "math error" if degree is equal to zero regardless of the
# radicant
#
# @return N-th root of radicant.


def u_rt(x, y):
    return set_err() if y == 0 or (x == 0 and y < 0) else round(x ** (1/y), 10)


##
# @brief Calculates the factorial of a number.
#
# @param n Number of which the factorial is counted
#
# @exception "math error" if the entered number isn't an integer
# @exception "math error" if the entered number is equal to or less than zero
#
# @return The factorial of number n.


def factorial(n):
    if n >= 0 and type(n) == int:
        res = 1
        for i in range(1, n + 1):
            res *= i
        return res
    return set_err()


##
# @}


##
# @defgroup converter Converter
# @brief Includes base converter
# @{


##
# @brief Converts numbers from base 10 numeral system into base 2, 8 and 16
# numeral systems.
#
# @param ex Number in base 10 system
# @param c Base system, into which the base 10 number is converted
#
# @warning "NUMBER IS TOO BIG, MAX 32 BIT" if number isnt within 32 bit range
# @warning "ONLY POSITIVE OR NEGATIVE INTEGERS" if a decimal number is
# entered
#
# @return The original number converted into different numeral systems.


def convert(ex, c):
    try:
        ex = int(ex)
        if ex >= -2147483648 and ex <= 4294967295:
            if ex < 0:
                if c == "oct":
                    return "-" + oct(ex)[3:]
                if c == "hex":
                    return "-" + hex(ex).upper()[3:]
                if c == "bin":
                    res = ["0", "b", "1"]
                    bin_num = bin(int(abs(ex)))
                    for i in range(2, len(bin_num)):
                        if bin_num[i] == "0":
                            res.append("1")
                        else:
                            res.append("0")
                    res = "".join(res)
                    res = bin(int(res, 2) + int("1", 2))
                    return res[2:]
            else:
                if c == "oct":
                    return oct(ex)[2:]
                if c == "hex":
                    return hex(ex).upper()[2:]
                if c == "bin":
                    return bin(ex)[2:]
        else:
            return "NUMBER IS TOO BIG, MAX 32 BIT"

    except:
        return "ONLY POSITIVE OR NEGATIVE INTEGERS"


##
# @}


##
# @defgroup parentheses Parentheses
# @brief Includes methods used for counting expresions inside brackets.
# @{


##
# @brief Takes expressions from parser, solves them one by one.
#
# @param expression Mathematical expression that needs to be solved
#
# @return Results from expressions in parentheses.


def execute(expression):

    def refresh(char):
        for i in range(len(ex) - 2):
            if ex[i] == "(" and ex[i+2] == ")":
                del ex[i]
                del ex[i + 1]
        return [i for i, e in enumerate(ex) if e == char]

    def exec_factorial(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = int(ex[symbolsPos[- 1] - 1])
            ex[symbolsPos[- 1] - 1] = str(factorial(first_num))
            del ex[symbolsPos[-1]]
            exec_factorial(refresh("!"))
        return

    def exec_upow(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[- 1] - 1])
            second_num = float(ex[symbolsPos[- 1] + 1])
            ex[symbolsPos[- 1] - 1] = str(u_pow(first_num, second_num))
            del ex[symbolsPos[- 1] + 1]
            del ex[symbolsPos[- 1]]
            exec_upow(refresh("^"))
        return

    def exec_urt(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[- 1] - 1])
            second_num = float(ex[symbolsPos[- 1] + 1])
            ex[symbolsPos[- 1] - 1] = str(u_rt(second_num, first_num))
            del ex[symbolsPos[- 1] + 1]
            del ex[symbolsPos[- 1]]
            exec_urt(refresh("√"))
        return

    def exec_div(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[- 1] - 1])
            second_num = float(ex[symbolsPos[- 1] + 1])
            ex[symbolsPos[- 1] - 1] = str(div(first_num, second_num))
            del ex[symbolsPos[- 1] + 1]
            del ex[symbolsPos[- 1]]
            exec_div(refresh("/"))
        return

    def exec_multip(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[- 1] - 1])
            second_num = float(ex[symbolsPos[- 1] + 1])
            ex[symbolsPos[- 1] - 1] = str(mul(first_num, second_num))
            del ex[symbolsPos[- 1] + 1]
            del ex[symbolsPos[- 1]]
            exec_multip(refresh("*"))
        return

    def exec_sub(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[0] - 1])
            second_num = float(ex[symbolsPos[0] + 1])
            ex[symbolsPos[0] - 1] = str(sub(first_num, second_num))
            del ex[symbolsPos[0] + 1]
            del ex[symbolsPos[0]]
            exec_sub(refresh("-"))
        return

    def exec_add(symbolsPos):
        if len(symbolsPos) > 0:
            first_num = float(ex[symbolsPos[0] - 1])
            second_num = float(ex[symbolsPos[0] + 1])
            ex[symbolsPos[0] - 1] = str(add(first_num, second_num))
            del ex[symbolsPos[0] + 1]
            del ex[symbolsPos[0]]
            exec_add(refresh("+"))
        return

    ex = list(filter(None, re.split("(\d+\.\d+|\d+|[^ 0-9])", expression)))
    if(ex[0] == "-"):
        temp = ex[1]
        del ex[0]
        ex[0] = "-{}".format(temp)
    while "-" in ex and ex[ex.index("-") - 1] in "+-*/":
        temp = ex[ex.index("-") + 1]
        index = ex.index("-")
        del ex[ex.index("-")]
        ex[index] = "-{}".format(temp)

    exec_factorial(refresh("!"))
    exec_upow(refresh("^"))
    exec_urt(refresh("√"))
    exec_div(refresh("/"))
    exec_multip(refresh("*"))
    exec_sub(refresh("-"))
    exec_add(refresh("+"))
    return ex


##
# @brief Takes an expression, separates individual parentheses
# from one another.
#
# @param expression Mathematical expression that needs to be solved
#
# @return Individual parentheses, gives them to the execute function
# which solves them one by one from the deepest one until they are all
# solved.


def parser(expression):
    pattern = re.compile(r"\s+")
    x = re.sub(pattern, "", expression)
    indexes = [i for i, y in enumerate(x) if y == "π"]
    offset = 0
    for i in indexes:
        if x[i - 1 + offset].isdigit() and i != 0:
            x = x[:i + offset] + "*" + x[i + offset:]
            offset += 1
    x = x.replace("π", "3.1415926535")
    indexes = [i for i, y in enumerate(x) if y == "√"]
    offset = 0
    for i in indexes:
        if not x[i - 1 + offset].isdigit() or i == 0:
            x = x[:i + offset] + "2" + x[i + offset:]
            offset += 1
    variables = {}
    pattern = re.compile(r"\([^()]*\)")
    avaib_variables = [i for i in range(65, 123) if i not in range(91, 97)]
    for var in avaib_variables:
        re_search = re.search(pattern, x)
        if re_search:
            x = x.replace(re_search.group(0), chr(var))
            variables[chr(var)] = re_search.group(0)
        else:
            stopFlag = var
            break
        stopFlag = var
    avaib_variables = [i for i in range(65, stopFlag) if i not in range(91,97)]
    for key in avaib_variables:
        variables[chr(key)] = re.sub("[\(\)]", "", variables[chr(key)])
        variables[chr(key)] = execute(variables[chr(key)])[0]
        for next_key in range(key, key+len(avaib_variables)):
            if next_key in avaib_variables:
                pattern = r"[{0}]".format(chr(key))
                variables[chr(next_key)] = re.sub(
                    pattern,
                    variables[chr(key)],
                    variables[chr(next_key)])

    for key in avaib_variables:
        pattern = r"[{0}]".format(chr(key))
        x = re.sub(pattern, variables[chr(key)], x)
    return execute(x)[0]


##
# @}
