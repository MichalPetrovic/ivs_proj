import math_lib
import sys


def profile(file_name):
	file = open(file_name, "r")
	arr = []
	for line in file:
		arr.append(int(line))
	sum_x = sum(arr)
	len_arr = len(arr)
	y = math_lib.parser("(1/{})*{}".format(len_arr, sum_x))
	sum_x_pow = sum([math_lib.u_pow(x, 2) for x in arr])
	s = math_lib.parser("√(1/({}-1)*({}-{}*{}^2))".format(len_arr, sum_x_pow, len_arr, y))
	return s

if __name__ == "__main__":
	if len(sys.argv)> 1:
		profile(sys.argv[1])
	else:
		print("You must specify a file!!")