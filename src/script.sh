mkdir -p ./installer/usr/share/calculator-SZF 2>/dev/null
mkdir -p ./installer/usr/share/applications 2>/dev/null
cp math_lib.py ./installer/usr/share/calculator-SZF/math_lib.py
cp main.py ./installer/usr/share/calculator-SZF/main.py && chmod +x ./installer/usr/share/calculator-SZF/main.py
cp favicon.png ./installer/usr/share/calculator-SZF/favicon.png
cp calculator_ui.py ./installer/usr/share/calculator-SZF/calculator_ui.py
mkdir -p ./installer/usr/local/bin 2>/dev/null
mkdir -p ./installer/tmp 2>/dev/null
cp requirements.txt ./installer/tmp/requirements.txt
cp ./installer/DEBIAN/calculator-SZF.desktop ./installer/usr/share/applications/calculator-SZF.desktop
chmod +x ./installer/DEBIAN/postinst
mv installer/calculator-SZF.exe calculator-SZF.exe
dpkg-deb --build ./installer/ ./installer/calculator-SZF.deb
mv calculator-SZF.exe installer/calculator-SZF.exe