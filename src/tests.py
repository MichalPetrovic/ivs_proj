#!/usr/bin/python3
# -*- coding: utf-8 -*-
import math_lib as math_lib
import doctest


def test_add(x, y):
    """
    # ZERO + ZERO
    >>> test_add(0, 0)
    0

    # POSITIVE NUMBERS
    >>> test_add(0, 4)
    4
    >>> test_add(8, 0)
    8
    >>> test_add(8, 4)
    12

    # NEGATIVE NUMBERS
    >>> test_add(0, -4)
    -4
    >>> test_add(-8, 0)
    -8
    >>> test_add(-25, 45)
    20
    >>> test_add(25, -45)
    -20
    >>> test_add(-50, -20)
    -70

    # DECIMAL NUMBERS
    >>> test_add(0, 5.3)
    5.3
    >>> test_add(5.3, 0)
    5.3
    >>> test_add(1.1, 1.1)
    2.2
    >>> test_add(1.1, 2.2)
    3.3
    >>> test_add(2.5, -1.1)
    1.4
    >>> test_add(-1.1, 2.5)
    1.4
    >>> test_add(-1.1, -2.5)
    -3.6
    """
    return math_lib.add(x, y)


def test_sub(x, y):
    """
    # ZERO - ZERO
    >>> test_sub(0, 0)
    0

    # POSITIVE NUMBERS
    >>> test_sub(0, 4)
    -4
    >>> test_sub(8, 0)
    8
    >>> test_sub(8, 4)
    4

    # NEGATIVE NUMBERS
    >>> test_sub(0, -45)
    45
    >>> test_sub(-25, 0)
    -25
    >>> test_sub(-25, 45)
    -70
    >>> test_sub(25, -45)
    70
    >>> test_sub(-50, -20)
    -30

    # DECIMAL NUMBERS
    >>> test_sub(0, 5.3)
    -5.3
    >>> test_sub(5.3, 0)
    5.3
    >>> test_sub(1.1, 2.5)
    -1.4
    >>> test_sub(2.5, 1.1)
    1.4
    >>> test_sub(-1.1, 2.5)
    -3.6
    >>> test_sub(-1.1, -2.5)
    1.4
    """
    return math_lib.sub(x, y)


def test_mul(x, y):
    """
    # ZERO - ZERO
    >>> test_mul(0, 0)
    0

    # POSITIVE NUMBERS
    >>> test_mul(0, 1)
    0
    >>> test_mul(1, 0)
    0
    >>> test_mul(1, 20)
    20
    >>> test_mul(20, 1)
    20
    >>> test_mul(5, 5)
    25

    # NEGATIVE NUMBERS
    >>> test_mul(0, -1)
    0
    >>> test_mul(-1, 0)
    0
    >>> test_mul(5, -5)
    -25
    >>> test_mul(-5, 5)
    -25
    >>> test_mul(-30,-20)
    600


    # DECIMAL NUMBERS
    >>> test_mul(1.5, 0)
    0.0
    >>> test_mul(0, 2.5)
    0.0
    >>> test_mul(1.5, 2.8)
    4.2
    >>> test_mul(1.5, 2.5)
    3.75
    >>> test_mul(1.5, -2.5)
    -3.75
    >>> test_mul(-1.5, 2.5)
    -3.75
    >>> test_mul(-1.5, -2.5)
    3.75

    """
    return math_lib.mul(x, y)


def test_div(x, y):
    """
    # ZERO / ZERO
    >>> test_div(0, 0)
    Traceback (most recent call last):
    Exception: math error

    # POSITIVE NUMBERS
    >>> test_div(0, 20)
    0.0
    >>> test_div(20, 0)
    Traceback (most recent call last):
    Exception: math error
    >>> test_div(1, 5)
    0.2
    >>> test_div(5, 1)
    5.0
    >>> test_div(50, 5)
    10.0

    # NEGATIVE NUMBERS
    >>> test_div(0, 20)
    0.0
    >>> test_div(20, 0)
    Traceback (most recent call last):
    Exception: math error
    >>> test_div(-1, 5)
    -0.2
    >>> test_div(5, -1)
    -5.0
    >>> test_div(-30, -5)
    6.0

    # DECIMAL NUMBERS
    >>> test_div(5.5, 2.2)
    2.5
    >>> test_div(-5.5, 2.2)
    -2.5
    >>> test_div(5.5, -2.2)
    -2.5
    >>> test_div(-5.5, -2.2)
    2.5
    """
    return math_lib.div(x, y)


def test_pow(x, y):
    """
    # 0 TO THE POWER OF 0
    >>> test_pow(0, 0)
    Traceback (most recent call last):
    Exception: math error

    # POSITIVE NUMBERS
    >>> test_pow(0, 5)
    0
    >>> test_pow(5, 0)
    1
    >>> test_pow(5, 1)
    5
    >>> test_pow(5, 2)
    25
    >>> test_pow(10,10)
    10000000000

    # NEGATIVE NUMBERS
    >>> test_pow(0, -5)
    Traceback (most recent call last):
    Exception: math error
    >>> test_pow(-5, 0)
    1
    >>> test_pow(-5, 2)
    25
    >>> test_pow(5, -2)
    0.04
    >>> test_pow(-5, 5)
    -3125
    >>> test_pow(-10,-10)
    1e-10
    """
    return math_lib.u_pow(x, y)


def test_sqrt(x, y):
    """
    # 0 SQRT BY 0
    >>> test_sqrt(0, 0)
    Traceback (most recent call last):
    Exception: math error

    # POSITIVE NUMBERS
    >>> test_sqrt(0, 5)
    0.0
    >>> test_sqrt(5, 0)
    Traceback (most recent call last):
    Exception: math error
    >>> test_sqrt(9, 2)
    3.0
    >>> test_sqrt(27, 3)
    3.0

    # NEGATIVE NUMBERS
    >>> test_sqrt(0, -5)
    Traceback (most recent call last):
    Exception: math error
    >>> test_sqrt(-5, 0)
    Traceback (most recent call last):
    Exception: math error
    >>> test_sqrt(5, -2)
    0.4472135955
    """
    return math_lib.u_rt(x, y)


def test_f(x):
    """
    # ZERO FACTORIAL
    >>> test_f(0)
    1

    # POSITIVE NUMBERS
    >>> test_f(2)
    2
    >>> test_f(5)
    120
    >>> test_f(10)
    3628800

    # NEGATIVE NUMBERS
    >>> test_f(-2)
    Traceback (most recent call last):
    Exception: math error

    # DECIMAL NUMBERS
    >>> test_f(5.2)
    Traceback (most recent call last):
    Exception: math error
    """
    return math_lib.factorial(x)


def test_parser(x):
    """
    >>> test_parser("(2+2) * (2+2)")
    '16.0'
    >>> test_parser("(1.5+2.8) * (1.5+2.8)")
    '18.49'

    >>> test_parser("(8-4) * (8-4)")
    '16.0'
    >>> test_parser("(3.5-1.7) * (3.5-1.7)")
    '3.24'

    >>> test_parser("(2+2)*2")
    '8.0'
    >>> test_parser("(6-3)*(1.5-0.8)")
    '2.1'
    >>> test_parser("(5.2+2.4)*3")
    '22.8'
    >>> test_parser("(5*5)-5")
    '20.0'
    >>> test_parser("(2.8*1.4)-1")
    '2.92'

    >>> test_parser("(2-6)*2")
    '-8.0'
    >>> test_parser("(-6-3)*(1.5-0.8)")
    '-6.3'
    >>> test_parser("(-5.2+2.4)*3")
    '-8.4'
    >>> test_parser("(-5*5)-5")
    '-30.0'
    >>> test_parser("(-2.8*1.4)-1")
    '-4.92'

    >>> test_parser("(2+2)/2")
    '2.0'
    >>> test_parser("(6-3)/(1.5-0.8)")
    '4.2857142857'
    >>> test_parser("(5.2+2.3)/3")
    '2.5'
    >>> test_parser("(5/5)-5")
    '-4.0'
    >>> test_parser("(2.8/1.4)-1")
    '1.0'

    >>> test_parser("(3^3 - 2)*2")
    '50.0'
    >>> test_parser("(4^4 - 200)+8")
    '64.0'
    >>> test_parser("(4^4 - 200)+(8^2)")
    '120.0'

    >>> test_parser("(2√9 - 2)+8")
    '9.0'
    >>> test_parser("(3√27 - 2)+8")
    '9.0'
    >>> test_parser("(3√27 - 2)+(2√9 -1)")
    '3.0'
    >>> test_parser("√9 + (25-6)*2")
    '41.0'

    >>> test_parser("(3! + 19) /5")
    '5.0'
    >>> test_parser("(5! + 30) /(3!)")
    '25.0'

    >>> test_parser("√π^2")
    '3.1415926535'
    >>> test_parser("2π + 5 + 3*π")
    '20.7079632675'
    >>> test_parser("√9*√4+2π-3π")
    '2.8584073465'
    """
    return math_lib.parser(x)


def test_convert(x, c):
    """
    >>> test_convert("1", "bin")
    '1'
    >>> test_convert("1", "oct")
    '1'
    >>> test_convert("1", "hex")
    '1'

    >>> test_convert("255", "bin")
    '11111111'
    >>> test_convert("255", "oct")
    '377'
    >>> test_convert("255", "hex")
    'FF'

    >>> test_convert("2147483647", "bin")
    '1111111111111111111111111111111'
    >>> test_convert("2147483647", "oct")
    '17777777777'
    >>> test_convert("2147483647", "hex")
    '7FFFFFFF'

    >>> test_convert("-1", "bin")
    '11'
    >>> test_convert("-1", "oct")
    '-1'
    >>> test_convert("-1", "hex")
    '-1'

    >>> test_convert("-255", "bin")
    '100000001'
    >>> test_convert("-255", "oct")
    '-377'
    >>> test_convert("-255", "hex")
    '-FF'

    >>> test_convert("-2147483647", "bin")
    '10000000000000000000000000000001'
    >>> test_convert("-2147483647", "oct")
    '-17777777777'
    >>> test_convert("-2147483647", "hex")
    '-7FFFFFFF'

    >>> test_convert("slovo", "bin")
    'ONLY POSITIVE OR NEGATIVE INTEGERS'
    >>> test_convert("slovo", "oct")
    'ONLY POSITIVE OR NEGATIVE INTEGERS'
    >>> test_convert("slovo", "hex")
    'ONLY POSITIVE OR NEGATIVE INTEGERS'

    >>> test_convert("2147483647222", "bin")
    'NUMBER IS TOO BIG, MAX 32 BIT'
    >>> test_convert("2147483647222", "oct")
    'NUMBER IS TOO BIG, MAX 32 BIT'
    >>> test_convert("2147483647222", "hex")
    'NUMBER IS TOO BIG, MAX 32 BIT'
    """
    return math_lib.convert(x, c)


if __name__ == "__main__":
    if doctest.testmod().failed == 0:
        print("ALL TEST OK")

